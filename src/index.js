import { GViewerSDK } from './libs/gviewer-sdk.mjs';

const sdk = window.sdk = new GViewerSDK();

const container = document.getElementById('iframe-container');
const loading = document.getElementById('loading');
const progress = document.getElementById('progress');
const buttons = document.getElementById('styles');

const onStyleChange = (e) => {
  const styleName = e.target.dataset['styleName'];

  if (!styleName) return;

  sdk.setState({
    camera: {
      view: { name: '主kv视角-背面', enabled: true }
    },
    model: {
      style: {
        name: styleName,
        enabled: true,
        transition: { enabled: true, duration: 0.5 }
      }
    }
  });
};

Array.prototype.forEach.call(buttons.children, (button) => {
  button.addEventListener('click', onStyleChange);
});

sdk.on('preload:progress', p => {
  progress.innerHTML = `${(p * 100).toFixed(0)}%`;
});

sdk.on('start', async () => {
  await new Promise(resolve => setTimeout(resolve, 500));

  await sdk.setState({ model: { style: { name: '锁屏_aeen', enabled: true } } });
  await sdk.setState({ camera: { orbitCamera: { useMouseInput: false, useTouchInput: false, inertiaFactor: 0 }}});
  await sdk.setState({ camera: { view: { name: '主kv', enabled: true } } });

  loading.style.display = 'none';

  await new Promise(resolve => setTimeout(resolve, 1000));

  await sdk.setState({ camera: { orbitCamera: { inertiaFactor: 0.4 }} });
  await sdk.setState({ camera: { view: { name: '主kv-结束', enabled: true }} });
  await sdk.setState({ model: { style: { name: `开屏_uk`, enabled: true, transition: { enabled: true, duration: 0.5 } }} });
  await sdk.setState({ camera: { orbitCamera: { useMouseInput: true, useTouchInput: true }}});
  await sdk.setState({ camera: { orbitCamera: { inertiaFactor: 0.12 }} });

  buttons.style.visibility = 'visible';
});

sdk.attach(container, { src: 'https://gizmohub.com/gviewer/page/027be392b35272d4614e0312de911d7a74f3e082' });
